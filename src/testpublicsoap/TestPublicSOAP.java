package testpublicsoap;

import java.lang.reflect.Array;
import java.util.List;

/**
 *
 * @author S_SAUKIN
 */
public class TestPublicSOAP {

    public static void main(String[] args) {

        List<TCurrency> a = listOfCurrenciesByName().getTCurrency();

//        for (int i = 0; i < a.size(); i++) {
//            if (i>0 && i!=(i-1)) {
//                System.out.println(a.get(i).getSName());
//            }
//        }
        for (TCurrency t : a) {
            System.out.println(t.getSName() + " / " + t.getSISOCode());
        }
        

    }

    private static ArrayOftCurrency listOfCurrenciesByName() {
        testpublicsoap.CountryInfoService service = new testpublicsoap.CountryInfoService();
        testpublicsoap.CountryInfoServiceSoapType port = service.getCountryInfoServiceSoap12();
        return port.listOfCurrenciesByName();
    }

}
